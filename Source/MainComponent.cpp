/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    addAndMakeVisible(&gui);

}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{
    gui.setSize(getWidth(), getHeight());

}


