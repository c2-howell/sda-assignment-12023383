//
//  PosMeter.h
//  JuceBasicWindow
//
//  Created by Ciaran Howell on 04/01/2015.
//
//

#ifndef __JuceBasicWindow__PosMeter__
#define __JuceBasicWindow__PosMeter__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "TimeClock.h"



class PosMeter: public Component

{
public:
    PosMeter();
    
    void resized() override;
    
    
    
    
    
    
private:
    ToggleButton meter[8];
    TimeClock clock;
    
};

#endif /* defined(__JuceBasicWindow__PosMeter__) */
