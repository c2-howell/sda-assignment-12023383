//
//  PlayStopGui.cpp
//  JuceBasicWindow
//
//  Created by Ciaran Howell on 04/01/2015.
//
//

#include "PlayStopGui.h"


PlayStopGui::PlayStopGui()
{
    playStopButton.setButtonText ("Play");
    playStopButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    playStopButton.setColour(TextButton::buttonColourId, Colours::grey);
    playStopButton.setColour(TextButton::buttonOnColourId, Colours::lightgrey);
    addAndMakeVisible (&playStopButton);
    playStopButton.addListener (this);
}

void PlayStopGui::resized()
{
    playStopButton.setBounds (0, 0, getWidth(), getHeight());
}

void PlayStopGui::buttonClicked(Button* startStopButton)
{
    if (onOff==0) {
        std::cout<<onOff;
        onOff++;
    }else if (onOff==1){
        std::cout<<onOff;
        onOff--;
    }
    
}

int PlayStopGui::getPlayStopStatus()
{
    return onOff;
}