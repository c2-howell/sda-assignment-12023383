//
//  GUI.h
//  JuceBasicWindow
//
//  Created by Ciaran Howell on 04/01/2015.
//
//

#ifndef __JuceBasicWindow__GUI__
#define __JuceBasicWindow__GUI__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "PosMeter.h"
#include "PlayStopGui.h"

class GUI : public Component

{
public:
    GUI();
    
    void resized ();
    
private:
    PosMeter posMeter;
    PlayStopGui playStopGui;
};

#endif /* defined(__JuceBasicWindow__GUI__) */
