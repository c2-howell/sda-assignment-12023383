//
//  PlayStopGui.h
//  JuceBasicWindow
//
//  Created by Ciaran Howell on 04/01/2015.
//
//

#ifndef __JuceBasicWindow__PlayStopGui__
#define __JuceBasicWindow__PlayStopGui__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "TimeClock.h"

class PlayStopGui :   public Component,
public Button::Listener



{
public:
    
    PlayStopGui();
    
    //Component
    void resized();
    
    void buttonClicked(Button* playStopButton) override;
    
    int getPlayStopStatus();
   
    
private:
    TimeClock clock;
    TextButton playStopButton;
    int onOff = 0;
    
};

#endif /* defined(__JuceBasicWindow__PlayStopGui__) */
