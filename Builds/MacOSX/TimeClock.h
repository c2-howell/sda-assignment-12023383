//
//  TimeClock.h
//  JuceBasicWindow
//
//  Created by Ciaran Howell on 04/01/2015.
//
//

#ifndef __JuceBasicWindow__TimeClock__
#define __JuceBasicWindow__TimeClock__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"


//==============================================================================
/*
 This component lives inside our window, and this is where you should put all
 your controls and content.
 */
class TimeClock   : public Timer

{
public:
    //==============================================================================
    TimeClock();
    ~TimeClock();
    
    void startClock();
    
    void stopClock();
    
    int getBeat();
    
    void timerCallback() override;
    
   
    
    
private:
    //==============================================================================
    int i;
    
    
    
   
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TimeClock)
    
};




#endif /* defined(__JuceBasicWindow__TimeClock__) */
