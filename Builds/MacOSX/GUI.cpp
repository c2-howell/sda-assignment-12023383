//
//  GUI.cpp
//  JuceBasicWindow
//
//  Created by Ciaran Howell on 04/01/2015.
//
//

#include "GUI.h"


GUI::GUI()
{
    addAndMakeVisible(playStopGui);
    addAndMakeVisible(posMeter);
    
}

void GUI::resized()
{
    playStopGui.setSize(getWidth(), 40);
    posMeter.setSize(getWidth(), getHeight());
}